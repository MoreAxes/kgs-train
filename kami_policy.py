#!/usr/bin/env python3

import sys
from time import time

import tensorflow as tf
import tensorflow

from tensorflow import keras
K = keras.backend
Model = keras.models.Model
Input = keras.layers.Input
Add = keras.layers.Add
Conv2D = keras.layers.Conv2D
Concatenate = keras.layers.Concatenate
Dense = keras.layers.Dense
Lambda = keras.layers.Lambda
LocallyConnected2D = keras.layers.LocallyConnected2D
Reshape = keras.layers.Reshape
Maximum = keras.layers.Maximum
Multiply = keras.layers.Multiply
Activation = keras.layers.Activation
RepeatVector = keras.layers.RepeatVector
ZeroPadding2D = keras.layers.ZeroPadding2D
Constraint = keras.constraints.Constraint

import kami_input as ki


class EightFoldSymmetry(Constraint):
  def __init__(self, perm):
    self.perm = perm
  def __call__(self, x):
    xt = tf.transpose(x, perm=self.perm)
    flips = [x, xt]
    flips.append(tf.reverse(x, [1]))
    flips.append(tf.reverse(xt, [1]))
    flips.append(tf.reverse(x, [2]))
    flips.append(tf.reverse(xt, [2]))
    flips.append(tf.reverse(x, [1, 2]))
    flips.append(tf.reverse(xt, [1, 2]))
    return sum(flips) / len(flips)

def constr(layer, kc, bc, *args, **kwargs):
  return layer(*args, **kwargs, kernel_constraint=kc, bias_constraint=bc)

def policy_net_model(x_tensor):
  inputs = Input(tensor=x_tensor[0])

  kc = Constraint() # ([1, 0, 2, 3])
  bc = Constraint()
  conv9 = constr(Conv2D, kc, bc, 16, [9, 9], padding="same", activation="elu", dtype="float32")(inputs)
  conv7 = constr(Conv2D, kc, bc, 16, [7, 7], padding="same", activation="elu", dtype="float32")(inputs)
  conv5 = constr(Conv2D, kc, bc, 16, [5, 5], padding="same", activation="elu", dtype="float32")(inputs)
  conv3 = constr(Conv2D, kc, bc, 16, [3, 3], padding="same", activation="elu", dtype="float32")(inputs)

  all_conv = Concatenate(3)([conv9, conv7, conv5, conv3])

  conv5_2 = constr(Conv2D, kc, bc, 8, [19, 19], padding="same", activation=None)(all_conv)
  # zero_pad = ZeroPadding2D((2, 2), data_format="channels_last")(conv5_2)
  # lcon5 = LocallyConnected2D(1, [5, 5], padding="valid")(zero_pad)
  # add = Add()([conv5_2, inputs])
  # reduce = Lambda(lambda x: tf.reduce_sum(x, [3], keepdims=True))(conv5_2)


  # conv9 = Conv2D(16, [9, 9], padding="same", activation="elu", dtype="float32")(reduce)
  # conv7 = Conv2D(16, [7, 7], padding="same", activation="elu", dtype="float32")(reduce)
  # conv5 = Conv2D(16, [5, 5], padding="same", activation="elu", dtype="float32")(reduce)
  # conv3 = Conv2D(16, [3, 3], padding="same", activation="elu", dtype="float32")(reduce)

  # all_conv = Concatenate(3)([conv9, conv7, conv5, conv3])

  # conv5_2 = Conv2D(8, [19, 19], padding="same", activation=None)(all_conv)
  # add = Add()([conv5_2, add])
  # reduce = Lambda(lambda x: tf.reduce_sum(x, [3], keepdims=True))(conv5_2)

  conv5_3 = Conv2D(1, [5, 5], padding="same", activation=None)(conv5_2)

  outputs = Reshape([361])(conv5_3)
  outputs = Activation("softmax")(outputs)

  return Model(inputs=inputs, outputs=outputs)


def policy_net_finalizer(m, binary):
  def one_hot_at_max(t):
    out = K.argmax(t)
    out = K.one_hot(out, 361)
    return out
  out = m.outputs[0]
  if binary:
    out = Lambda(one_hot_at_max)(m.outputs[0])
  out = Reshape((19, 19), name="reshape_finalize")(out)
  ext_forbidden = Input((19, 19), name="ext_forbidden")
  known_forbidden = tf.reduce_sum(tf.slice(m.inputs[0], [0, 0, 0, 0], [-1, -1, -1, 7]), axis=3, name="known_forbidden")
  forbidden = Maximum(name="forbidden_merge")([ext_forbidden, known_forbidden])

  ones = -tf.ones([19, 19], name="ones")
  allowed = Add(name="forbidden_neg")([ones, forbidden])
  out = Multiply(name="forbidden_apply")([out, allowed])

  return Model(inputs=(m.inputs + [ext_forbidden, known_forbidden, ones]), outputs=out)


def train(filename, out_filename=None):
  data = ki.default_iterator(filename).get_next()
  model = policy_net_model(data)
  model.summary()
  # keras.utils.plot_model(model, to_file="last.png", show_shapes=True)
  # opt = keras.optimizers.Adam(decay=0.004, amsgrad=True)
  # tensorboard = keras.callbacks.TensorBoard(log_dir="logs/{}".format(time()))
  model.compile(optimizer="adam", loss="kullback_leibler_divergence", metrics=["categorical_accuracy"], target_tensors=[data[1]])
  try:
    model.fit(steps_per_epoch=100, epochs=300, verbose=2)
  except KeyboardInterrupt:
    pass

  if out_filename is not None:
    model.save(out_filename)

  return model


def evaluate_batches(model, filename, batches=None, batch_size=100):
  data = ki.default_generator(filename, batch_size=batch_size)

  print("batch#", end="\t")
  for n in model.metrics_names:
    print(n, end="\t")
  print()

  it = 0
  for batch in data:
    it += 1
    print(it, end="\t")
    for metric in model.test_on_batch(batch[0], batch[1]):
      print(metric, end="\t")
    print()

    if batches is not None and it >= batches:
      break


def predict_batches(model, filename, batches=None, batch_size=1):
  data = ki.default_generator(filename, batch_size=batch_size)

  it = 0
  for batch in data:
    it += 1
    this = tf.reduce_sum(tf.slice(batch[0], [0, 0, 0, 0], [1, 19, 19, 3]), axis=-1)
    other = tf.reduce_sum(tf.slice(batch[0], [0, 0, 0, 3], [1, 19, 19, 3]), axis=-1)
    board = this + 2 * other

    print("input:\n{}".format(K.get_session().run(board)))
    print("prediction:\n{}".format(model.predict_on_batch(batch[0])))

    if batches is not None and it >= batches:
      break


if __name__ == "__main__":
  train(sys.argv[1], sys.argv[2])
