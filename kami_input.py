"""Routine for decoding the KGS binary file format from u-go."""

import os

import tensorflow as tf

K = tf.keras.backend

def dataset(filename):
  tag_bytes = 2
  label_bytes = 4
  planes_bytes = 316
  record_bytes = tag_bytes + label_bytes + planes_bytes

  def decode_example(example):
    example = tf.decode_raw(example, out_type=tf.uint8)[tag_bytes:]
    label_tensor = tf.bitcast(example[:label_bytes], tf.int32)
    example = example[label_bytes:]
    masks = tf.constant([1 << 7, 1 << 6, 1 << 5, 1 << 4, 1 << 3, 1 << 2, 1 << 1, 1 << 0], dtype=tf.uint8)
    exps = tf.constant([7, 6, 5, 4, 3, 2, 1, 0], dtype=tf.uint8)
    planes_all = tf.bitwise.right_shift(tf.bitwise.bitwise_and(tf.tile(tf.reshape(example, [planes_bytes, -1]), [1, 8]), masks), exps)
    # The 7-plane chunk is zero-padded to the next byte, so it needs slicing
    # (it's always just one bit off)
    planes_all = tf.reshape(planes_all, [-1])
    planes_all = planes_all[:7*19*19]
    planes_all = tf.reshape(planes_all, [7, 19, 19])
    planes_all = tf.cast(planes_all, tf.float32)
    return (tf.transpose(tf.stack(
        [
          planes_all[0],
          planes_all[1],
          planes_all[2],
          planes_all[3],
          planes_all[4],
          planes_all[5],
          planes_all[6],
          tf.ones([19, 19], dtype=tf.float32)
        ]), perm=[1, 2, 0]),
        tf.one_hot(label_tensor, 361, dtype=tf.float32))

  return tf.data.FixedLengthRecordDataset(
      filename,
      record_bytes=record_bytes,
      header_bytes=1024,
      footer_bytes=3).map(decode_example)


def default_iterator(filename, batch_size=1000):
  return dataset(filename).cache().repeat().shuffle(2*batch_size).batch(batch_size).make_one_shot_iterator()

def default_generator(filename, batch_size=1000):
  iterator = default_iterator(filename, batch_size)
  next_batch = iterator.get_next()
  # return iterator
  while True:
    yield K.get_session().run(next_batch)
  # print(next_batch)
  # return next_batch
