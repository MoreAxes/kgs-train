#!/usr/bin/env python3

import tensorflow as tf
import kami_policy as kp

import sys


if len(sys.argv) < 3:
  print("need 2 args")
  sys.exit(2)

m = tf.keras.models.load_model(sys.argv[1]);

out = m.outputs[0]
out = tf.reshape(out, [19, 19], name="reshape_finalize")
ext_forbidden = tf.placeholder(tf.float32, (19, 19), name="ext_forbidden")
known_forbidden = tf.reduce_sum(tf.slice(m.inputs[0], [0, 0, 0, 0], [-1, -1, -1, 7]), axis=3, name="known_forbidden")
forbidden = tf.maximum(ext_forbidden, known_forbidden, name="forbidden_merge")

ones = -tf.ones([19, 19], name="ones")
allowed = tf.add(ones, forbidden, name="forbidden_neg")
out = tf.multiply(out, allowed, name="forbidden_apply")
print("output name:", out.op.name)

# tf.train.write_graph(tf.get_default_graph(), "./", sys.argv[2], as_text=False)

saver = tf.train.Saver()
saver.save(tf.keras.backend.get_session(), sys.argv[2] + ".ckpt")

# final_m = kp.policy_net_finalizer(m, False)
# final_m.save(sys.argv[2])
