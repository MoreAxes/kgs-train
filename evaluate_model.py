#!/usr/bin/env python3

import kami_input as ki
import kami_policy as kp

import tensorflow
from tensorflow import keras

import sys

m = keras.models.load_model(sys.argv[1])
m.summary()
for l in m.layers:
  print("{}: {}".format(l.name, l.get_config()))
print(m.optimizer.get_config())
kp.evaluate_batches(m, sys.argv[2], 10, 10000)
