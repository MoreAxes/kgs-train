#!/usr/bin/env python

import kami_input as ki
import kami_policy as kp

import tensorflow
from tensorflow import keras
import numpy as np
np.set_printoptions(threshold=np.nan)

import sys

m = keras.models.load_model(sys.argv[1])
m = kp.policy_net_finalizer(m, True)
kp.predict_batches(m, sys.argv[2], 1, 1)
